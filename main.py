import sys
import mainCenterMgr
from PySide2 import QtWidgets

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    window = mainCenterMgr.MainWindow()
    window.show()
    sys.exit(app.exec_())

